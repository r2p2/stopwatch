# libstopwatch

[![pipeline status](https://gitlab.com/r2p2/stopwatch/badges/master/pipeline.svg)](https://gitlab.com/r2p2/stopwatch/commits/master)
[![coverage report](https://gitlab.com/r2p2/stopwatch/badges/master/coverage.svg)](https://gitlab.com/r2p2/stopwatch/commits/master)

## Build

```sh
mkdir build
cd build
cmake ..
make
```
