#include <stopwatch/laps.h>

namespace stopwatch
{

void Laps::add(duration_t const& duration)
{
	lap_durations_.emplace_back(duration);
}

Laps::duration_t Laps::avg() const
{
	if (is_empty()) {
		return duration_t{};
	}

	return sum() / size();
}

Laps::duration_t Laps::min() const
{
	if (is_empty()) {
		return duration_t{};
	}

	return *std::min_element(
		lap_durations_.begin(),
		lap_durations_.end());
}

Laps::duration_t Laps::max() const
{
	if (is_empty()) {
		return duration_t{};
	}

	return *std::max_element(
		lap_durations_.begin(),
		lap_durations_.end());
}

Laps::duration_t Laps::sum() const
{
	return std::accumulate(
		lap_durations_.begin(),
		lap_durations_.end(),
		duration_t{});
}

bool Laps::is_empty() const
{
	return lap_durations_.empty();
}

Laps::container_t::size_type Laps::size() const
{
	return lap_durations_.size();
}

} // namespace stopwatch
