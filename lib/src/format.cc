#include <stopwatch/format.h>
#include <stopwatch/units.h>

namespace stopwatch
{

std::string to_string(Units units)
{
	return
		std::to_string(units.h().count())  + "h " +
		std::to_string(units.m().count())  + "m " +
		std::to_string(units.s().count())  + "s " +
		std::to_string(units.ms().count()) + "ms " +
		std::to_string(units.us().count()) + "us " +
		std::to_string(units.ns().count()) + "ns";
}

} // namespace stopwatch
