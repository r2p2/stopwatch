#include <stopwatch/chronometer.h>

namespace stopwatch
{

Laps::duration_t Chronometer::avg() const
{
	return laps_->avg();
}

Laps::duration_t Chronometer::min() const
{
	return laps_->min();
}

Laps::duration_t Chronometer::max() const
{
	return laps_->max();
}

Laps::duration_t Chronometer::sum() const
{
	return laps_->sum();
}

Laps::container_t::size_type Chronometer::lap_count() const
{
	return laps_->size();
}

} // namespace stopwatch
