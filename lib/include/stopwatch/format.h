#pragma once

#include "units.h"

#include <string>

namespace stopwatch
{

std::string to_string(Units);

} // namespace stopwatch
