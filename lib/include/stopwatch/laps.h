#pragma once

#include <algorithm>
#include <chrono>
#include <numeric>   // std::accumulate
#include <vector>

namespace stopwatch
{

class Laps
{
public:
	using duration_t = std::chrono::nanoseconds;
	using container_t = std::vector<duration_t>;

	void add(duration_t const& duration);

	duration_t avg() const;
	duration_t min() const;
	duration_t max() const;
	duration_t sum() const;

	bool is_empty() const;
	container_t::size_type size() const;

private:
	container_t lap_durations_ = {};
};

} // namespace stopwatch
