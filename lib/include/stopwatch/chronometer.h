#pragma once

#include "lap.h"

namespace stopwatch
{

class Chronometer
{
public:
	template<class clock_t>
	Lap<clock_t> start()
	{
		return Lap<clock_t>(laps_);
	}

	Laps::duration_t avg() const;
	Laps::duration_t min() const;
	Laps::duration_t max() const;
	Laps::duration_t sum() const;
	Laps::container_t::size_type lap_count() const;

private:
	std::shared_ptr<Laps> laps_ = std::make_shared<Laps>();
};

} // namespace stopwatch
