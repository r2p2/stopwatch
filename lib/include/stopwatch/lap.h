#pragma once

#include "laps.h"

#include <memory>

namespace stopwatch
{

template <class clock_t>
class Lap
{
public:
	Lap() = delete;
	Lap(Lap const&) = delete;
	Lap(Lap&&) = default;

	Lap(std::weak_ptr<Laps> laps)
	: laps_(std::move(laps))
	, start_(clock_t::now())
	{
	}

	~Lap()
	{
		if (auto laps = laps_.lock()) {
			laps->add(clock_t::now() - start_);
		}
	}

	Lap& operator=(Lap const&) = delete;
	Lap& operator=(Lap&&) = default;

private:
	std::weak_ptr<Laps>          laps_;
	typename clock_t::time_point start_;
};

} // namespace stopwatch
