#pragma once

#include "laps.h"

namespace stopwatch
{

class Units
{
public:
	using hours        = std::chrono::hours;
	using minutes      = std::chrono::minutes;
	using seconds      = std::chrono::seconds;
	using milliseconds = std::chrono::milliseconds;
	using microseconds = std::chrono::microseconds;
	using nanoseconds  = std::chrono::nanoseconds;

	Units() = delete;
	Units(Laps::duration_t const& dur)
	: h_(std::chrono::duration_cast<hours>(dur))
	, m_(std::chrono::duration_cast<minutes>(dur - h_))
	, s_(std::chrono::duration_cast<seconds>(dur - h_ - m_))
	, ms_(std::chrono::duration_cast<milliseconds>(dur - h_ - m_ - s_))
	, us_(std::chrono::duration_cast<microseconds>(dur - h_ - m_ - s_ - ms_))
	, ns_(std::chrono::duration_cast<nanoseconds>(dur - h_ - m_ - s_ - ms_ - us_))
	{
	}

	Units(Units const&) = default;
	Units(Units&&) = default;

	Units& operator=(Units const&) = default;
	Units& operator=(Units&&) = default;

	hours h() const
	{
		return h_;
	}

	minutes m() const
	{
		return m_;
	}

	seconds s() const
	{
		return s_;
	}

	milliseconds ms() const
	{
		return ms_;
	}

	microseconds us() const
	{
		return us_;
	}

	nanoseconds ns() const
	{
		return ns_;
	}

private:
	hours        h_;
	minutes      m_;
	seconds      s_;
	milliseconds ms_;
	microseconds us_;
	nanoseconds  ns_;
};

} // namespace stopwatch
