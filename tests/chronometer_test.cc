#include "catch.hpp"

#include <stopwatch/chronometer.h>

#include "fake_clock.h"

using namespace std::chrono;

TEST_CASE("chronometer", "[chronometer]")
{
	stopwatch::Chronometer cm;

	SECTION("newly created chronometer")
	{
		REQUIRE(cm.lap_count() == 0);
		REQUIRE(cm.sum().count() == decltype(cm.sum()){}.count());
		REQUIRE(cm.avg().count() == decltype(cm.sum()){}.count());
		REQUIRE(cm.min().count() == decltype(cm.sum()){}.count());
		REQUIRE(cm.max().count() == decltype(cm.sum()){}.count());
	}

	SECTION("incomplete lap does not count")
	{
		auto lap = cm.start<FakeClock>();

		REQUIRE(cm.lap_count() == 0);
		REQUIRE(cm.sum().count() == decltype(cm.sum()){}.count());
		REQUIRE(cm.avg().count() == decltype(cm.sum()){}.count());
		REQUIRE(cm.min().count() == decltype(cm.sum()){}.count());
		REQUIRE(cm.max().count() == decltype(cm.sum()){}.count());
	}

	SECTION("one lap completed in no time")
	{
		{
			auto lap = cm.start<FakeClock>();
		}

		REQUIRE(cm.lap_count() == 1);
		REQUIRE(cm.sum().count() == decltype(cm.sum()){}.count());
		REQUIRE(cm.avg().count() == decltype(cm.sum()){}.count());
		REQUIRE(cm.min().count() == decltype(cm.sum()){}.count());
		REQUIRE(cm.max().count() == decltype(cm.sum()){}.count());
	}

	SECTION("two laps completed in no time")
	{
		{
			auto lap = cm.start<FakeClock>();
		}

		{
			auto lap = cm.start<FakeClock>();
		}

		REQUIRE(cm.lap_count() == 2);
		REQUIRE(cm.sum().count() == decltype(cm.sum()){}.count());
		REQUIRE(cm.avg().count() == decltype(cm.sum()){}.count());
		REQUIRE(cm.min().count() == decltype(cm.sum()){}.count());
		REQUIRE(cm.max().count() == decltype(cm.sum()){}.count());
	}

	SECTION("lap completions")
	{
		auto const dur1    = std::chrono::milliseconds(5);
		auto const dur2    = std::chrono::minutes(13);
		auto const dur_sum =
			std::chrono::duration_cast<decltype(cm.sum())>(dur1 + dur2);
		auto const avg     = dur_sum / 2;

		FakeClock::progress(std::chrono::hours(5));

		{
			auto lap = cm.start<FakeClock>();
			FakeClock::progress(dur1);
		}

		FakeClock::progress(std::chrono::seconds(22));

		{
			auto lap = cm.start<FakeClock>();
			FakeClock::progress(dur2);
		}

		REQUIRE(cm.lap_count() == 2);
		REQUIRE(cm.sum().count() == dur_sum.count());
		REQUIRE(cm.avg().count() == decltype(cm.sum()){avg}.count());
		REQUIRE(cm.min().count() == decltype(cm.sum()){dur1}.count());
		REQUIRE(cm.max().count() == decltype(cm.sum()){dur2}.count());
	}

	SECTION("bad api design allows for overlapping laps")
	{
		auto const dur1    = std::chrono::milliseconds(5);
		auto const dur2    = std::chrono::minutes(13);
		auto const dur_sum =
			std::chrono::duration_cast<decltype(cm.sum())>(
				dur1 + dur2 * 2);
		auto const avg     = dur_sum / 2;

		FakeClock::progress(std::chrono::hours(5));

		{
			auto lap1 = cm.start<FakeClock>();
			FakeClock::progress(dur1);

			auto lap2 = cm.start<FakeClock>();
			FakeClock::progress(dur2);
		}

		REQUIRE(cm.lap_count() == 2);
		REQUIRE(cm.sum().count() == dur_sum.count());
		REQUIRE(cm.avg().count() == decltype(cm.sum()){avg}.count());
		REQUIRE(cm.min().count() == decltype(cm.sum()){dur2}.count());
		REQUIRE(
			cm.max().count() ==
			decltype(cm.sum()){dur1 + dur2}.count());
	}
}
