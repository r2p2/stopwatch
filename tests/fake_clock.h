#include <chrono>

class FakeClock
{
public:
	using rep        = std::chrono::steady_clock::rep;
	using period     = std::chrono::steady_clock::period;
	using duration   = std::chrono::steady_clock::duration;
	using time_point = std::chrono::steady_clock::time_point;

	static time_point now()
	{
		return time_point{_time};
	}

	static void reset()
	{
		_time = duration{};
	}

	static void progress(duration const& delta)
	{
		_time += delta;
	}

	static void progress(duration&& delta)
	{
		_time += std::move(delta);
	}

private:
	static duration _time;
};

FakeClock::duration FakeClock::_time = FakeClock::duration{};
