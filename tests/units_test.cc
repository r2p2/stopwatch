#include "catch.hpp"

#include <stopwatch/units.h>

TEST_CASE("units", "[units]")
{
	using hours        = int;
	using minutes      = int;
	using seconds      = int;
	using milliseconds = int;
	using microseconds = int;
	using nanoseconds  = int;

	std::vector<
		std::tuple<
			hours,
			minutes,
			seconds,
			milliseconds,
			microseconds,
			nanoseconds
			>> test_collection {
		{ 0, 0, 0, 0, 0, 0 },
		{ 1, 0, 0, 0, 0, 0 },
		{ 0, 1, 0, 0, 0, 0 },
		{ 0, 0, 1, 0, 0, 0 },
		{ 0, 0, 0, 1, 0, 0 },
		{ 0, 0, 0, 0, 1, 0 },
		{ 0, 0, 0, 0, 0, 1 },
		{ 1, 2, 3, 4, 5, 6 },
	};

	for (auto const& test_data : test_collection) {
		auto const exp_h = std::get<0>(test_data);
		auto const exp_m = std::get<1>(test_data);
		auto const exp_s = std::get<2>(test_data);
		auto const exp_ms = std::get<3>(test_data);
		auto const exp_us = std::get<4>(test_data);
		auto const exp_ns = std::get<5>(test_data);

		auto const dur =
			std::chrono::hours{exp_h} +
			std::chrono::minutes{exp_m} +
			std::chrono::seconds{exp_s} +
			std::chrono::milliseconds{exp_ms} +
			std::chrono::microseconds{exp_us} +
			std::chrono::nanoseconds{exp_ns};

		auto const unit = stopwatch::Units{dur};

		REQUIRE(unit.h().count() == exp_h);
		REQUIRE(unit.m().count() == exp_m);
		REQUIRE(unit.s().count() == exp_s);
		REQUIRE(unit.ms().count() == exp_ms);
		REQUIRE(unit.us().count() == exp_us);
		REQUIRE(unit.ns().count() == exp_ns);
	}
}
