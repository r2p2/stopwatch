#include <stopwatch/chronometer.h>
#include <stopwatch/format.h>

#include <chrono>
#include <iostream>

int main()
{
	stopwatch::Chronometer cm;
	for (int i = 0; i < 127; ++i) {
		auto lap = cm.start<std::chrono::steady_clock>();

		for (int j = 0; j < 1000000; ++j);
	}

	std::cout << "lap count: " << cm.lap_count() << std::endl;
	std::cout << " duration: " << stopwatch::to_string(cm.sum()) << std::endl;
	std::cout << "      avg: " << stopwatch::to_string(cm.avg()) << std::endl;
	std::cout << "      min: " << stopwatch::to_string(cm.min()) << std::endl;
	std::cout << "      max: " << stopwatch::to_string(cm.max()) << std::endl;

	return 0;
}
